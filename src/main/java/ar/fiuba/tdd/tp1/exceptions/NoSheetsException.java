package ar.fiuba.tdd.tp1.exceptions;

/**
 * Usada cuando se quiere usar/acceder/eliminar una Sheet que no existe.
 */
public class NoSheetsException extends Exception {
    @Override
    public String getMessage() {
        return "There is no such Sheet";
    }
}
