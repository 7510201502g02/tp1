package ar.fiuba.tdd.tp1.model.formulas;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;
import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.formatters.Formatter;
import ar.fiuba.tdd.tp1.model.formatters.FormatterFactory;
import ar.fiuba.tdd.tp1.model.types.ContentType;
import ar.fiuba.tdd.tp1.model.types.ContentTypeFactory;
import ar.fiuba.tdd.tp1.parsers.CollaborativeParserOrderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leandro on 17-Oct-15. :)
 *
 * Concrete class that implements the interface and functional Formula models the behavior of the contents of a cell, can be evaluated and may be altered in the formula it contains. Stored representation of the formula that currently contains a reference to the representation corresponding to Formula object and in which delegates its own assessment. Also it contains a list of formats and their options, which were assigned and a reference to the Formatter object resulting from the construction based on the composition thereof .
 */
public class CellContent implements Formula {

    List<String[]> formatsOfContent = new ArrayList<>();
    private transient Formula containedFormula = new ValueFormula(0);
    private String formulaRepresentation = "";
    private ContentType contentType = new ContentTypeFactory().getContentType("default");
    private transient Formatter formatter = new FormatterFactory().getTypeInfependentFormatter();

    public void setFormatter(String... formatOptions) {
        try {
            this.formatter = new FormatterFactory().getFormatter(formatter, formatOptions);
            formatsOfContent.add(formatOptions);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

    public String getValueAsString() throws InvalidFormatException, InvalidCellReferenceException {
        try {
            return contentType.getContentAsString(formulaRepresentation, containedFormula.eval().toString(),
                    formatter.format(containedFormula.eval()));
        } catch (InvalidFormulaException e) {
            return "";
        }
    }

    public double getValueAsDouble() throws InvalidFormulaException, InvalidFormatException, InvalidCellReferenceException {
        return contentType.getContentAsDouble(eval());
    }

    public void setContent(Book book, final String representation) {
        formulaRepresentation = representation;
        containedFormula = new CollaborativeParserOrderer().getCollaborativeParser(book).parse(representation);
    }

    public void setFormulaRepresentation(String representation) {
        formulaRepresentation = representation;
    }

    public void updateContent(Book book) {
        if (!formulaRepresentation.equals("")) {
            this.setContent(book, formulaRepresentation);
        }
    }

    @Override
    public Object eval() throws InvalidFormulaException, InvalidFormatException, InvalidCellReferenceException {
        return containedFormula.eval();
    }

    public final String getRepresentation() {
        return formulaRepresentation;
    }


    public void setContentType(String contentType) {
        this.contentType = new ContentTypeFactory().getContentType(contentType);
    }

}