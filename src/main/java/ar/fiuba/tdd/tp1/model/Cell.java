package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;
import ar.fiuba.tdd.tp1.model.formulas.CellContent;
import ar.fiuba.tdd.tp1.model.formulas.Formula;

/**Class that models the behavior of a cell which contains an instance of CellContent and a name.
 */
public class Cell {

    private String name;
    private CellContent content;

    /**
     * Cell constructor. It's Formula is "".
     *
     * @param name the Cell's name
     */
    public Cell(final String name) {
        this.name = name;
        content = new CellContent();
    }

    public String getName() {
        return name;
    }

    /**
     * Returns the String representation of the Formula.
     *
     * @return a String rrepresenting the Formula
     */
    public String getFormula() {
        return content.getRepresentation();
    }


    /**
     * Returns the String representation of the Content of the Cell.
     *
     * @return a String representing the Content of the Cell.
     */
    public String getContent() throws InvalidFormatException, InvalidCellReferenceException {
        return content.getValueAsString();
    }

    /**
     * Sets a new Formula to the Cell.
     *
     * @param book       Book instance that contains the Cell.
     * @param expression String input
     */
    public void setFormula(Book book, final String expression) {
        content.setContent(book, expression);
    }

    /**
     * Returns the Formula.
     *
     * @return Formula's instance of the Cell.
     */
    public Formula getReference() {
        return content;
    }

    /**
     * Calculates and returns the actual numerical value of the Cell.
     *
     * @return the value as a Double
     * @throws InvalidFormulaException If the Formula is invalid.
     */
    public double getValue() throws InvalidFormulaException, InvalidFormatException, InvalidCellReferenceException {
        return content.getValueAsDouble();
    }

    public void setType(String type) {
        this.content.setContentType(type);
    }

    public void setFormatter(String[] formatter) {
        content.setFormatter(formatter);
    }


    public void updateContent(Book book) {
        this.content.updateContent(book);
    }
}
