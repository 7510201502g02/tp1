package ar.fiuba.tdd.tp1.model.formulas;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;

/**
 * Created by Leandro on 03-Oct-15. :)
 *
 * Functional interface that encapsulates the behavior being assessed itself a formula.
 */
public interface Formula {
    /**
     * Evaluates the Formula and returns it.
     *
     * @return The result of calculating the Formula.
     * @throws InvalidFormulaException if the Formula is Invalid.
     */
    Object eval() throws InvalidFormulaException, InvalidFormatException, InvalidCellReferenceException;
}
