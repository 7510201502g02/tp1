package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.exceptions.*;
import ar.fiuba.tdd.tp1.model.formulas.Formula;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Representa una Hoja de calculo que contiene muchas Celdas.
 */
public class HashMappedSheet implements Sheet {

    private String sheetName = "default";
    private int sheetID = 1;
    private Map<String, Cell> cells = new HashMap<>();
    private int maxRow = 1;
    private char maxCol = 'A';

    /**
     * Constructs the Sheet with "default" name.
     */
    public HashMappedSheet() {
    }

    /**
     * HashMappedSheet constructor.
     *
     * @param sheetName the name of the Sheet
     * @param sheetID   the ID of the Sheet
     */
    public HashMappedSheet(String sheetName, int sheetID) {
        this.sheetName = sheetName;
        this.sheetID = sheetID;
    }

    @Override
    public String getName() {
        return sheetName;
    }

    @Override
    public int getID() {
        return sheetID;
    }

    @Override
    public Cell getCell(String cellName) {
        try {
            return putIfAbsent(cellName);
        } catch (NoCellException e) {
            return cells.get(cellName);
        }
    }


    public int getMaxRow() {
        return maxRow;
    }

    public List<String> getRowAsString(int row) {
        List<String> rowValues = new ArrayList<>();
        for (char col = 'A'; col <= maxCol; col++) {
            try {
                rowValues.add(getCell("" + col + row).getContent());
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                rowValues.add("");
            }
        }
        return rowValues;
    }

    @Override
    public double getValueOf(String cellName) throws NoValueException, InvalidFormulaException, NoCellException,
            InvalidFormatException, InvalidCellReferenceException {

        return putIfAbsent(cellName).getValue();
    }

    /**
     * Returns the Formula instance of the Cell.
     *
     * @param cellName the name of the Cell
     * @return the Formula instance of the Cell
     */
    @Override
    public Formula getReferenceOf(String cellName) throws NoCellException {
        return putIfAbsent(cellName).getReference();
    }

    public List<Formula> getRangeReferencesOf(String[] cellIni, String[] cellFin) {
        List<Formula> subFormulas = new ArrayList<>();
        if (cellIni[0].charAt(0) > cellFin[0].charAt(0)) {
            char temp = cellFin[0].charAt(0);
            cellFin[0] = String.valueOf(cellIni[0].charAt(0));
            cellIni[0] = String.valueOf(temp);
        }
        if (Integer.parseInt(cellIni[1]) > Integer.parseInt(cellFin[1])) {
            String temp = cellFin[1];
            cellFin[1] = cellIni[1];
            cellIni[1] = temp;
        }

        for (char i = cellIni[0].charAt(0); i <= cellFin[0].charAt(0); i++) {
            for (int j = Integer.parseInt(cellIni[1]); j <= Integer.parseInt(cellFin[1]); j++) {
                Formula formula = cells.get("" + i + j).getReference();
                subFormulas.add(formula);
            }
        }
        return subFormulas;
    }

    private Cell putIfAbsent(String cellName) throws NoCellException {
        /* Chequeo que sea <LETRA><NUMERO> */
        if (cellName.length() > 1
                && Character.isAlphabetic(cellName.codePointAt(0))
                && Character.isDigit(cellName.codePointAt(1))) {
            Cell newCell = new Cell(cellName);
            if (cells.putIfAbsent(cellName, newCell) == null) {
                updateMaxColAndRow(cellName);
                return newCell;
            } else {
                return cells.get(cellName);
            }
        }
        throw new NoCellException();
    }

    private void updateMaxColAndRow(String cellName) {
        //System.out.println("CELDA AGREGADA: "+cellName);
        char col = cellName.charAt(0);
        int row = Integer.parseInt(cellName.substring(1));
        if (col > maxCol) {
            maxCol = col;
        }
        if (row > maxRow) {
            maxRow = row;
        }

        //System.out.println("NUEVO MAX ROW: "+maxRow);
        //System.out.println("NUEVO MAX COL: "+maxCol);
    }

    public void updateCellsContent(Book book) {
        for (Cell cell : cells.values()) {
            book.setWorkingCell(cell);
            cell.updateContent(book);
        }
    }
}
