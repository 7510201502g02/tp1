package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.exceptions.*;
import ar.fiuba.tdd.tp1.model.formulas.Formula;

import java.util.List;

/*
 * Representa una Hoja de calculo que contiene muchas Celdas
 * y puede devolverlas, asignarle Formulas y obtener sus valores
 */
public interface Sheet {
    /**
     * Returns the Cell.
     *
     * @param cellName the Cell's name.
     * @return the Cell instance.
     */
    Cell getCell(String cellName) throws NoCellException;

    //void setFormula(String cellName, String formula);

    /**
     * Gets the numerical value of the Cell.
     *
     * @param cellName the Cell's name
     * @return the calculated value of the Cell
     * @throws NoValueException        if there is NO formula in  the Cell
     * @throws InvalidFormulaException If the Formula is not valid
     */
    double getValueOf(String cellName) throws NoValueException,
            InvalidFormulaException, InvalidFormatException, NoCellException, InvalidCellReferenceException;

    /**
     * Returns the Formula instance contained in that Cell.
     *
     * @param cellName the Cell's name
     * @return the Formula instance
     */
    Formula getReferenceOf(String cellName) throws NoCellException;

    List<Formula> getRangeReferencesOf(String[] cellIni, String[] cellFin);

    String getName();

    int getID();
}
