package ar.fiuba.tdd.tp1.model.formatters;

import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Leandro on 19-Oct-15. :)
 *
 * Concrete class that models the behavior of instantiation of objects from Formatter options parameterize the behavior of the object created and an identifier of the type of formatting to conduct the object. Contains references to default Formatter and a dictionary of FormatterCreators in which delegates the creation of the corresponding Formatter object. Sets the defaults FormatterCreators initially with generic algorithms that will be used to parameterize the Formatter and sets the result field on which take effect.
 */
public class FormatterFactory {

    final String valueFieldId;
    final String symbolFieldId;
    private final Formatter typeInfependentFormatter;
    private Map<String, FormatterCreator> formatterCreators;

    public FormatterFactory() {
        valueFieldId = "value";
        symbolFieldId = "symbol";
        formatterCreators = new HashMap<>();
        typeInfependentFormatter = new Formatter(valueFieldId, null) {

            @Override
            String originalFormatter(Object content, String... modifiedFields) {
                return "";
            }

            @Override
            String modifyFormat(Object content, String originalFormat) {
                return content.toString();
            }
        };
        addDefaultFormatterCreators();
    }

    public Formatter getTypeInfependentFormatter() {
        return typeInfependentFormatter;
    }

    public Formatter getFormatter(final Formatter oldFormatter, String... options) throws InvalidFormatException {
        if (formatterCreators.containsKey(options[0])) {
            return formatterCreators.get(options[0]).makeFormatter(oldFormatter, options);
        } else {
            return oldFormatter;
        }
    }

    public void addFormatterCreator(final FormatterCreator formatterCreator, final String contentType) {
        formatterCreators.put(contentType, formatterCreator);
    }

    private DecimalFormat decimalFormatOfNDecimals(int num) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(num);
        df.setMinimumFractionDigits(num);
        return df;
    }

    public void addDefaultFormatterCreators() {

        addFormatterCreator((oldFormatter, options) -> new Formatter(valueFieldId, oldFormatter) {
            @Override
            String modifyFormat(Object content, String originalFormat) {
                return originalFormat + new SimpleDateFormat(options[1].replace("D", "d")).format(content);
            }
        }, "format");

        addFormatterCreator((oldFormatter, options) -> new Formatter(symbolFieldId, oldFormatter) {
            @Override
            String modifyFormat(Object content, String originalFormat) {
                return options[1] + " " + originalFormat;
            }
        }, "symbol");

        addFormatterCreator((oldFormatter, options) -> new Formatter(valueFieldId, oldFormatter) {
            @Override
            String modifyFormat(Object content, String originalFormat) {
                return originalFormat + decimalFormatOfNDecimals(Integer.parseInt(options[1])).format(content);
            }
        }, "decimal");
    }

}
