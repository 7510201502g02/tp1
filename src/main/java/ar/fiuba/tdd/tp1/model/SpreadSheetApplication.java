package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.commands.CommandsHandler;
import ar.fiuba.tdd.tp1.commands.NothingToRedoException;
import ar.fiuba.tdd.tp1.commands.NothingToUndoException;
import ar.fiuba.tdd.tp1.exceptions.*;
import ar.fiuba.tdd.tp1.persistence.JsonParser;
import ar.fiuba.tdd.tp1.persistence.PersistenceParser;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ale on 10/3/15.
 */
public class SpreadSheetApplication {

    private transient CommandsHandler actions = new CommandsHandler();

    private ArrayList<Book> books = new ArrayList<>();

    private transient PersistenceParser parser = new JsonParser();

    public SpreadSheetApplication() {
    }

    public void restart() {
        actions.dropEverething();
        books = new ArrayList<>();
    }

    /**
     * Adds a new Book.
     *
     * @param bookName the Book's name
     */
    public void addBook(String bookName) {
        actions.addNewBook(books, bookName);
    }

    /**
     * Removes a Book.
     *
     * @param bookName the Book's name
     * @throws NoBookException if there is no such Book
     */
    public void removeBook(String bookName) throws NoBookException {
        actions.removeBook(books, getBook(bookName));
    }

    /**
     * Returns the Book.
     *
     * @param bookName the Book's name
     * @return the Book instance.
     * @throws NoBookException if there is no such Book
     */
    public Book getBook(String bookName) throws NoBookException {
        for (Book book : books) {
            if (book.getName().equals(bookName)) {
                return book;
            }
        }
        throw new NoBookException();
    }

    /**
     * Returns an ArrayList of the Book's names.
     *
     * @return an ArrayList of the Book's names
     */
    public ArrayList<String> getBooksNames() {
        ArrayList<String> names = new ArrayList<>();
        for (Book book : books) {
            names.add(book.getName());
        }
        return names;
    }

    /**
     * Returns an ArrayList of the Book's Sheet's names.
     *
     * @param bookName the name of the Book.
     * @return an ArrayList of the Book's Sheet's names.
     */
    public ArrayList<String> getSheetsNamesOfBook(String bookName) {
        try {
            return this.getBook(bookName).getSheetsNames();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Adds a new sheet to the Book.
     *
     * @param bookName  the Book's name
     * @param sheetName the Sheet's name
     * @throws NoBookException if there is no such Book
     */
    public void addSheet(String bookName, String sheetName) throws NoBookException {
        actions.addSheet(getBook(bookName), sheetName);
    }

    /**
     * Removes a Sheet from the Book.
     *
     * @param bookName  the Book's name
     * @param sheetName the Sheet's name
     * @throws NoBookException if there is no such Book
     */
    public void removeSheet(String bookName, String sheetName) throws NoBookException {
        actions.removeSheet(getBook(bookName), sheetName);
    }

    /**
     * Sets the Formula of a Cell.
     *
     * @param bookName  the Book's name
     * @param sheetName the Sheet's name
     * @param cellName  the Cell's name
     * @param formula   the String input
     * @throws NoSheetsException if there is no such Sheet
     * @throws NoBookException   if there is no such Book
     */
    public void setFormulaOf(String bookName, String sheetName, String cellName, String formula) throws NoSheetsException, NoBookException {
        Book book = getBook(bookName);
        book.setWorkingSheet(sheetName);
        Cell cell = book.getCell(sheetName, cellName);
        //book.setWorkingCell(cell); lo pongo en el CellSetFormulaCommand
        actions.setFormulaOf(book, cell, formula);
    }

    /**
     * Returns the numerical value of the Cell
     *
     * @param bookName  the Book's name
     * @param sheetName the Sheet's name
     * @param cell      the Cell's name
     * @return the calculated Value of the Cell's Formula.
     * @throws NoValueException        If there is No Formula in the Cell.
     * @throws InvalidFormulaException if the formula is invalid
     * @throws NoSheetsException       if there is No such Sheet
     * @throws NoBookException         if there is no such Book
     */
    public double getValueOf(String bookName, String sheetName, String cell) throws NoValueException, InvalidFormulaException,
            NoSheetsException, NoBookException, InvalidFormatException, InvalidCellReferenceException, NoCellException {
        return getBook(bookName).getValueOf(sheetName, cell);
    }


    /**
     * Returns the Cell's Formula as a String
     *
     * @param bookName  the Book's name
     * @param sheetName the Sheet's name
     * @param cell      the Cell's name
     * @return the String representation Cell's Formula.
     * @throws NoValueException        If there is No Formula in the Cell.
     * @throws InvalidFormulaException if the formula is invalid
     * @throws NoSheetsException       if there is No such Sheet
     * @throws NoBookException         if there is no such Book
     */
    public String getFormulaAsString(String bookName, String sheetName, String cell) throws NoValueException, InvalidFormulaException,
            NoSheetsException, NoBookException {
        return getBook(bookName).getCell(sheetName, cell).getFormula();
    }


    /**
     * Returns the Cell's Content as a String
     *
     * @param bookName  the Book's name
     * @param sheetName the Sheet's name
     * @param cell      the Cell's name
     * @return the String representation Cell's Content.
     * @throws NoValueException        If there is No Formula in the Cell.
     * @throws InvalidFormulaException if the formula is invalid
     * @throws NoSheetsException       if there is No such Sheet
     * @throws NoBookException         if there is no such Book
     */
    public String getCellContentAsString(String bookName, String sheetName, String cell) throws NoValueException, InvalidFormulaException,
            NoSheetsException, NoBookException, InvalidFormatException, InvalidCellReferenceException {
        return getBook(bookName).getCell(sheetName, cell).getContent();
    }


    /** Returns the Formula of the Cell.
     * @param bookName the Book's name
     * @param sheetName the Sheet's name
     * @param cellName the Cell's name
     * @return the instance of the Formula in the Cell.
     * @throws NoSheetsException if there is no such Sheet
     * @throws NoBookException if there is no such Book
     *
    public Formula getReferenceOf(String bookName, String sheetName, String cellName) throws NoSheetsException, NoBookException {
    return getBook(bookName).getReferenceOf(sheetName, cellName);
    }
     */
    /**
     * Undoes the last action.
     *
     * @throws NothingToUndoException if there is nothing to undo
     */
    public void undo() throws NothingToUndoException {
        actions.undo();
    }

    /**
     * Redoes the last undone action.
     *
     * @throws NothingToRedoException if there is nothing tho Redo
     */
    public void redo() throws NothingToRedoException {
        actions.redo();
    }

    public void setTypeOf(String workBookName, String workSheetName, String cellId, String cellType)
            throws NoBookException, NoSheetsException {
        getBook(workBookName).getCell(workSheetName, cellId).setType(cellType);
    }

    public void setFormatterOf(String workBookName, String workSheetName, String cellId, String... options)
            throws NoBookException, NoSheetsException {
        getBook(workBookName).getCell(workSheetName, cellId).setFormatter(options);
    }

    /**
     * Replaces the actual Book (discarding changes) with the retrievedBook.
     *
     * @param retrievedBook a Book retrieved from persistence.
     */
    public void setRetrievedBook(Book retrievedBook) {
        books.removeIf(book -> book.getName().equals(retrievedBook.getName()));
        books.add(retrievedBook);
    }

    public void persistSpreadSheet(String outPutFile) {
        parser.dump(this, outPutFile);
    }

    public SpreadSheetApplication reloadSpreadSheet(String persistedFile) {
        return parser.retrieveSpreadSheet(persistedFile);
    }

    public void persistBook(String bookName, String outPutFile) throws NoBookException {
        parser.dumpBook(this, bookName, outPutFile);
    }

    public void reloadBook(String persistedFile) {
        Book retrievedBook = parser.retrieveBook(persistedFile);
        setRetrievedBook(retrievedBook);
    }

    public int getMaxRowsOfSheet(String book, String sheet) {
        try {
            return getBook(book).getMaxRowsOfSheet(sheet);
        } catch (Exception e) {
            return -1;
        }
    }

    public List<String> getRowValuesOf(String book, String sheet, int row) throws NoBookException, NoSheetsException {
        return getBook(book).getRowValuesOf(sheet, row);
    }
}
