package ar.fiuba.tdd.tp1.model;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedMultigraph;

/**
 * Created by Leandro on 24-Oct-15. :)
 * Concrete class that implements the interface CellReferenceValidator from validating the references do not form cycles in a graph that is internally structured . The graph is constructed with cells as vertices and for each validated reference edges.
 *
 */
public class GraphCellReferenceValidator implements CellReferenceValidator {

    private DirectedGraph<String, DefaultEdge> referenceGraph =
            new DirectedMultigraph<>(
                    DefaultEdge.class);


    @Override
    public void addValidReference(String startsSheetName, String startsCellName, String endSheetName, String endCellName)
            throws InvalidCellReferenceException {
        final String startCellVertexName = startsSheetName + startsCellName;
        final String endCellVertexName = endSheetName + endCellName;
        referenceGraph.addVertex(startCellVertexName);
        referenceGraph.addVertex(endCellVertexName);
        referenceGraph.addEdge(startCellVertexName, endCellVertexName);
        if (new CycleDetector<>(referenceGraph).detectCycles()) {
            referenceGraph.removeEdge(startsSheetName + startsCellName, endSheetName + endCellName);
            throw new InvalidCellReferenceException();
        }
    }
}
