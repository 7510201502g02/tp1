package ar.fiuba.tdd.tp1.model.formulas;

/**
 * Created by Leandro on 03-Oct-15. :)
 *
 *
 Concrete class that implements the Formula functional interface and model the behavior of a formula consisting of a single term that contains a value .
 */
public class ValueFormula implements Formula {

    private Object formulaValue;

    /**
     * ValueFormula constructor.
     *
     * @param value the value.
     */
    public ValueFormula(final Object value) {
        this.formulaValue = value;
    }

    @Override
    public final Object eval() {
        return formulaValue;
    }
}
