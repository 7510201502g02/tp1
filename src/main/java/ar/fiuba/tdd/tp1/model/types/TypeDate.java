package ar.fiuba.tdd.tp1.model.types;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ale on 10/26/15.
 */
public class TypeDate implements ContentType {
    String typeName = "Date";
    private SimpleDateFormat dateDefaultFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
    private SimpleDateFormat dateRequiredFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    @Override
    public String getContentAsString(String originalInput, String contentRepresentation, String formattedContent) {
        Date date;
        try {
            //si no es fecha, retorno error.
            date = dateDefaultFormat.parse(contentRepresentation);
        } catch (Exception e) {
            return "Error:BAD_DATE";
        }
        //si es fecha y esta en el formato default, lo paso al requerido
        if (contentRepresentation.equals(formattedContent)) {
            return dateRequiredFormat.format(date);
        }
        return formattedContent;
    }

    @Override
    public String getType() {
        return typeName;
    }
}
