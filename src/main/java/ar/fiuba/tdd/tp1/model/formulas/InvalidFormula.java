package ar.fiuba.tdd.tp1.model.formulas;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;

/**
 * Created by Leandro on 03-Oct-15. :)
 *
 * Concrete class that implements the interface and functional Formula models the behavior of an invalid formula .
 */
public class InvalidFormula implements Formula {

    @Override
    public Object eval() throws InvalidFormulaException, InvalidCellReferenceException {
        throw new InvalidFormulaException();
    }
}
