package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.Cell;

/**
 * Created by ale on 10/3/15.
 */
public class CellSetFormulaCommand implements Command {

    private final Book book;
    private final Cell cell;
    private final String formula;
    private final String lastFormula;

    /**
     * CellSetFormulaCommand constructor.
     *
     * @param book    the Book instance of the Cell
     * @param cell    the Cell instance to set the formula
     * @param formula the formula as a String
     */
    public CellSetFormulaCommand(Book book, Cell cell, String formula) {
        this.cell = cell;
        this.formula = formula;
        this.lastFormula = cell.getFormula();
        this.book = book;
    }

    @Override
    public void execute() {
        book.setWorkingCell(cell);
        cell.setFormula(book, formula);
    }

    @Override
    public void undo() {
        cell.setFormula(book, lastFormula);
    }
}
