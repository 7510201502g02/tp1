package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.HashMappedSheet;

/**
 * Created by ale on 10/4/15.
 */
public class RemoveSheetCommand implements Command {
    HashMappedSheet sheet = null;
    String sheetName;
    Book spreadSheet;

    /**
     * RemoveSheetCommand constructor.
     *
     * @param book  the Book that contains the Sheet
     * @param sheet the name of the Sheet to remove
     */
    public RemoveSheetCommand(Book book, String sheet) {
        this.sheetName = sheet;
        this.spreadSheet = book;
    }

    @Override
    public void execute() {
        try {
            sheet = spreadSheet.removeSheet(sheetName);
        } catch (NoSheetsException e) {
            e.printStackTrace();//no hacer nada...
        }
    }

    @Override
    public void undo() {
        spreadSheet.addSheet(sheet.getID(), sheet);
    }
}
