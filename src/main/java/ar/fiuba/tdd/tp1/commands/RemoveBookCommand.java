package ar.fiuba.tdd.tp1.commands;

import ar.fiuba.tdd.tp1.model.Book;

import java.util.ArrayList;

/**
 * Created by ale on 10/5/15.
 */
public class RemoveBookCommand implements Command {
    private final ArrayList<Book> books;
    private final Book book;

    /**
     * RemoveBookCommand constructo.
     *
     * @param books A Collection of Books
     * @param book  A Book instance to remove from de Collection
     */
    public RemoveBookCommand(ArrayList<Book> books, Book book) {
        this.books = books;
        this.book = book;
    }

    @Override
    public void execute() {
        books.remove(book);
    }

    @Override
    public void undo() {
        books.add(book);
    }
}
