package ar.fiuba.tdd.tp1.commands;

/*Command interface to implement the Command Design Pattern
 *
 */
public interface Command {
    /**
     * Executes the Command.
     */
    public void execute();

    /**
     * Sets the Command's main attribute to it's previous state.
     */
    public void undo();
}
