package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.view.ConsoleView;

/**
 * Created by ale on 10/22/15.
 */
public class Main {
    public static void main(String[] args) {
        SpreadSheetApplication appp = new SpreadSheetApplication();
        appp.addBook("book1");
        try {
            appp.addSheet("book1", "sheet1");
        } catch (NoBookException e) {
            e.printStackTrace();
        }
        ConsoleView view = new ConsoleView(appp);
        view.init();
    }
}
