package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.formulas.Formula;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leandro on 17-Oct-15. :)
 *
 CollaborativeParser subclass that implements the functionality of parsing the symbol that identifies the formulas or in its absence indicates that the term is a value. From determine FormulaParser determine the next call .
*/

public class SyntaxCollaborativeParser extends CollaborativeParser {

    private FormulaParser valueParser;

    public SyntaxCollaborativeParser(OperationCollaborativeParser operationsParser, FormulaParser valueParser) {
        super(operationsParser);
        this.valueParser = valueParser;
    }

    @Override
    public String getSymbolToParse() {
        return "=";
    }

    @Override
    public List<Formula> getSubFormulas(final List<String> subExpressions) {
        /** Si la expresion correspondia a una formula por tener el =,
         * entonces se parsean las operaciones,
         * sino unicamente se parsean los valores.
         */
        List<Formula> subFormulas = new ArrayList<>();
        if (subExpressions.size() > 1) {
            subFormulas.add(collaborativeParse(subExpressions.get(1)));
        } else {
            subFormulas.add(valueParser.parse(subExpressions.get(0)));
        }
        return subFormulas;
    }

    public List<String> preserveSymbol(List<String> subExpressions) {
        return subExpressions;
    }

}



