package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.exceptions.InvalidCellReferenceException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormatException;
import ar.fiuba.tdd.tp1.exceptions.InvalidFormulaException;
import ar.fiuba.tdd.tp1.model.formulas.Formula;

import java.util.List;
import java.util.ListIterator;

/**
 * Created by Leandro on 03-Oct-15. :)
 *
 * CollaborativeParser subclass that implements the functionality of instantiating objects that model the behavior of a composite by a mathematical operation between terms and provide the symbol for the operation which is applied in that formula object Formula.
 */
public class OperationCollaborativeParser extends CollaborativeParser {

    private String symbolToParse;
    private Operation operationToAplly;

    /**
     * OperationCollaborativeParser constructor
     *
     * @param symbol    a Symbol of the operand used to parse
     * @param operation an operation to apply to the operands
     * @param parser    a parser to continue.
     */
    public OperationCollaborativeParser(final String symbol,
                                        final Operation operation,
                                        final FormulaParser parser) {
        super(parser);
        symbolToParse = symbol;
        operationToAplly = operation;
    }


    public String getSymbolToParse() {
        return symbolToParse;
    }

    public Formula makeFormula(final List<Formula> subFormulas) {
        return () -> {
            try {
                    /*evaluo la formula entre el primer
                     operando (sub formula)
                    y todos los restantes
                     */
                ListIterator<Formula> iterator = subFormulas.listIterator();
                Object result = iterator.next().eval();
                while (iterator.hasNext()) {
                    result = operationToAplly.operate(result, iterator.next().eval());
                }
                return result;
            } catch (InvalidCellReferenceException e2) {
                throw new InvalidCellReferenceException();
            } catch (InvalidFormatException e) {
                throw new InvalidFormatException();
            } catch (Exception e) {
                throw new InvalidFormulaException();
            }
        };
    }
}
