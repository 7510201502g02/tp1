package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.model.formulas.ValueFormula;

/**
 * Created by Leandro on 03-Oct-15. :)
 *
 * Concrete class that implements the FormulaParser functional interface and is responsible for parsing values.
 */
public class ValueParser implements FormulaParser {
    @Override
    public Formula parse(final String expression) {
        try {
            if (expression.contains(".")) {
                return new ValueFormula(
                        Double.parseDouble(expression.replace(" ", "")));
            } else {
                return new ValueFormula(
                        Integer.parseInt(expression.replace(" ", "")));
            }
        } catch (NumberFormatException e) {
            return new ValueFormula(expression);
        }

    }
}
