package ar.fiuba.tdd.tp1.parsers;

/**
 * Created by Leandro on 03-Oct-15. :)
 *
 *
 Functional interface that encapsulates the behavior of a mathematical operation calculable present in a formula .
 */
public interface Operation {
    /**
     * Calculates the result of the operation betwen the first and second operands.
     *
     * @param firstOperand  the first operand
     * @param secondOperand the second operand
     * @return the result as a Double.
     */
    Object operate(final Object firstOperand, final Object secondOperand);
}
