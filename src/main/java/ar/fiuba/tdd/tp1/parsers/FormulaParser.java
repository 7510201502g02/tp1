package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.formulas.Formula;

/**
 * Created by Leandro on 03-Oct-15. :)
 *
 Functional interface that encapsulates the functionality of obtaining a formula from a mathematical expression.
 */
public interface FormulaParser {

    /**
     * parsea una expresion para constituir una estructura de formulas
     * contenidas en dicha expresion.
     *
     * @param expression an expression of a Formula to parse
     * @return the Formula instance of the parsed expression.
     */
    Formula parse(final String expression);
}
