package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.formulas.Formula;

import java.util.List;

/**
 * Created by Leandro on 03-Oct-15. :)
 *
 * Concrete class that implements the functionality to sort collaborative links between parsers to respect the mathematical sense of order among the terms.
 */
public class CollaborativeParserOrderer {
    /**
     * CollaborativeParserOrderer getter.
     *
     * @param book the Book to give to the ReferenceCollaborativeParser.
     * @return an OperationCollaborativeParser instance depending on the operation.
     */
    public CollaborativeParser getCollaborativeParser(Book book) {
        final Operation addition = (a1, b1) -> ((Number) a1).doubleValue() + ((Number) b1).doubleValue();
        final Operation subtraction = (a1, b1) -> ((Number) a1).doubleValue() - ((Number) b1).doubleValue();
        final Operation min = (a1, b1) -> Math.min(((Number) a1).doubleValue(), ((Number) b1).doubleValue());
        final Operation max = (a1, b1) -> Math.max(((Number) a1).doubleValue(), ((Number) b1).doubleValue());
        final Operation concat = (a1, b1) -> (a1.toString() + b1.toString());
        CollaborativeParser valueParsers = new DateCollaborativeParser("yyyy-MM-dd'T'HH:mm:ss'Z'", new ValueParser());

        FormulaParser loQueSePasaDeLosMaxCaracteres = new RangeOperationCollaborativeParser(":", min, book,
                new SetOperationCollaborativeParser("AVERAGE", addition,
                        new RangeOperationCollaborativeParser(":", addition, book,
                                new ReferenceCollaborativeParser(".", valueParsers, book)
                        ) {
                            @Override
                            public Formula makeFormula(List<Formula> subFormulas) {
                                return () -> {
                                    if (subFormulas.size() > 1) {
                                        return ((Number) super.makeFormula(subFormulas).eval()).doubleValue() / subFormulas.size();
                                    } else {
                                        return subFormulas.get(0).eval();
                                    }
                                };
                            }
                        }
                )
        );

        return new SyntaxCollaborativeParser(
                new OperationCollaborativeParser("+", addition,
                        new OperationCollaborativeParser("-", subtraction,
                                new SetOperationCollaborativeParser("CONCAT", concat,
                                        new RangeOperationCollaborativeParser(":", concat, book,
                                                new SetOperationCollaborativeParser("MAX", max,
                                                        new RangeOperationCollaborativeParser(":", max, book,
                                                                new SetOperationCollaborativeParser("MIN", min,
                                                                        loQueSePasaDeLosMaxCaracteres
                                                                )
                                                        )
                                                )
                                        )
                                )
                        )
                ), valueParsers);
    }
}
