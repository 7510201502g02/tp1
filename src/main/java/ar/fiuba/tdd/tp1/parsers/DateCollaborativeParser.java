package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.formulas.Formula;
import ar.fiuba.tdd.tp1.model.formulas.ValueFormula;

import java.text.SimpleDateFormat;

/**
 * Created by Leandro on 20-Oct-15. :)
 *
 * CollaborativeParser subclass that implements the functionality of parsing dates.
 */
public class DateCollaborativeParser extends CollaborativeParser {

    private final String dateSymbolFormat;

    /**
     * CollaborativeParser constructor.
     *
     * @param parser           a FormulaParser instance.
     * @param dateSymbolFormat symbol of the format in which the dates will be parsed.
     */
    public DateCollaborativeParser(String dateSymbolFormat, FormulaParser parser) {
        super(parser);
        this.dateSymbolFormat = dateSymbolFormat;
    }

    @Override
    public String getSymbolToParse() {
        return dateSymbolFormat;
    }

    @Override
    public Formula parse(String expression) {
        try {
            return new ValueFormula(new SimpleDateFormat(getSymbolToParse()).parse(expression));
        } catch (Exception e) {
            return super.parse(expression);
        }
    }

}
