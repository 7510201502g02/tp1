package ar.fiuba.tdd.tp1.parsers;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.formulas.Formula;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leandro on 18-Oct-15. :)
 *
 * OperationCollaborativeParser subclass that implements the functionality of the cell ranges parse and apply an operation between all references to cell formulas for the range.
 */
public class RangeOperationCollaborativeParser extends OperationCollaborativeParser {

    private Book book;

    public RangeOperationCollaborativeParser(String symbol, Operation operation, Book book, FormulaParser parser) {
        super(symbol, operation, parser);
        this.book = book;
    }

    @Override
    public List<Formula> getSubFormulas(final List<String> subExpressions) {
        List<Formula> subFormulas;
        if (subExpressions.size() > 1) {
            subFormulas = new ArrayList<>();
            String sub1 = subExpressions.get(0);
            String sub2 = subExpressions.get(1);
            String[] cell1 = sub1.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
            String[] cell2 = sub2.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
            try {
                subFormulas = book.getRangeReferenceOf(cell1, cell2);
            } catch (Exception e) {
                subFormulas.add(collaborativeParse(subExpressions.get(0) + getSymbolToParse() + subExpressions.get(1)));
            }
        } else {
            subFormulas = new ArrayList<>();
            subFormulas.add(collaborativeParse(subExpressions.get(0)));
        }
        return subFormulas;
    }
}
