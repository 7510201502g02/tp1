package ar.fiuba.tdd.tp1.parsers;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Leandro on 18-Oct-15. :)
 *
 * OperationCollaborativeParser subclass that implements the functionality of parsing operations applied to a set of ranks or terms.
 */
public class SetOperationCollaborativeParser extends OperationCollaborativeParser {
    /**
     * SetOperationCollaborativeParser constructor
     *
     * @param symbol    a Symbol of the operand used to parse
     * @param operation an operation to apply to the operands
     * @param parser    a parser to continue.
     */
    public SetOperationCollaborativeParser(String symbol, Operation operation, FormulaParser parser) {
        super(symbol, operation, parser);
    }

    @Override
    public String getRegularExpressionToParse() {
        return Pattern.quote(getSymbolToParse() + "(") + "|" + Pattern.quote(",") + "|" + Pattern.quote(")");
    }

    @Override
    public List<String> preserveSymbol(List<String> subExpressions) {
        subExpressions.remove("");
        subExpressions.remove(" ");
        return subExpressions;
    }
}
