package ar.fiuba.tdd.tp1.view;

import ar.fiuba.tdd.tp1.commands.NothingToRedoException;
import ar.fiuba.tdd.tp1.commands.NothingToUndoException;
import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.exceptions.NoValueException;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;

import java.util.HashMap;
import java.util.Scanner;
import java.util.function.Function;

/**
 * Created by ale on 10/22/15.
 */
public class ConsoleView {
    private final SpreadSheetApplication app;
    //private final Console cmd;
    private HashMap<String, Function> actions;

    public ConsoleView(SpreadSheetApplication app) {
        this.app = app;
        actions = new HashMap<String, Function>();
        addActions();
    }

    private void addActions() {
        addAddBookAction();
        addAddShhetAction();
        addPrintBooksAction();
        addPrintCellValueAction();
        addPrintSheetAction();
        addPrintSheetsOfBookAction();
        addRedoAction();
        addUndoAction();
        addSetCellValueAction();
    }

    public void init() {
        String input = "";
        String[] separated;
        String action = "";
        String args = "";
        while (!input.equals("exit")) {
            separated = input.split(" ");
            action = separated[0];
            if (separated.length > 1) {
                args = separated[1];
            }
            if (actions.containsKey(action)) {
                actions.get(action).apply(args);
            }
            input = new Scanner(System.in, "utf-8").nextLine();
        }
    }

    private void addAddShhetAction() {
        /*addSheet workBook-sheetName*/
        actions.put("addSheet", new Function() {
            @Override
            public Object apply(Object obj) {
                String arguments = (String) obj;
                String[] args = arguments.split("-");
                // String workBook = args[0];
                // String workSheet = args[1];
                try {
                    app.addSheet(args[0], args[1]);
                } catch (NoBookException e) {
                    System.out.print("No existe ese libro!!\n");
                }
                return null;
            }
        });
    }

    private void addAddBookAction() {
        /*addBook bookName*/
        actions.put("addBook", new Function() {
            @Override
            public Object apply(Object obj) {
                String arguments = (String) obj;
                String[] args = arguments.split("-");
                //String bookName = args[0];
                app.addBook(args[0]);
                return null;
            }
        });
    }

    private void addSetCellValueAction() {
        /*setCellValue workBook-workSheet-cellName-value*/
        actions.put("setCellValue", new Function() {
            @Override
            public Object apply(Object obj) {
                String arguments = (String) obj;
                String[] args = arguments.split("-");
                //String workBook = args[0];
                // String workSheet = args[1];
                //String cellName = args[2];
                //String value = args[3];
                try {
                    app.setFormulaOf(args[0], args[1], args[2], args[3]);
                } catch (NoSheetsException e) {
                    System.out.print("No hay esa hoja!\n");
                } catch (NoBookException e) {
                    System.out.print("No hay ese libro!\n");
                }
                return null;
            }
        });
    }

    private void addUndoAction() {
        /*undo*/
        actions.put("undo", new Function() {
                    @Override
                    public Object apply(Object obj) {
                        try {
                            app.undo();
                        } catch (NothingToUndoException e) {
                            System.out.print("No hay nada que deshacer!\n");
                        }
                        return null;
                    }
                }
        );
    }

    private void addRedoAction() {
        /*redo*/
        actions.put("redo", new Function() {
                    @Override
                    public Object apply(Object obj) {
                        try {
                            app.redo();
                        } catch (NothingToRedoException e) {
                            System.out.print("No hay nada que rehacer!\n");
                        }
                        return null;
                    }
                }
        );
    }

    private void addPrintSheetAction() {
        /*printSheet workBook-workSheet-cantFilas-cantColumnas*/
        actions.put("printSheet", new Function() {
            @Override
            public Object apply(Object obj) {
                String arguments = (String) obj;
                String[] args = arguments.split("-");
                //String workBook = args[0];
                // String workSheet = args[1];
                int rowsToPrint = Integer.parseInt(args[2]);
                int colsToPrint = Integer.parseInt(args[3]);

                printSheet(colsToPrint, rowsToPrint, args[0], args[1]);

                return null;
            }
        });
    }

    private void printSheet(int colsToPrint, int rowsToPrint, String workBook, String workSheet) {
        /*Imprimo encabezado*/
        System.out.print("   ");
        for (int col = 1; col <= colsToPrint; ++col) {
            System.out.print(getCharForNumber(col));
            System.out.print("   ");
        }
        System.out.println("");
        /*Imprimo tabla*/
        for (int col = 1; col <= colsToPrint; ++col) {
            System.out.print(String.valueOf(col));
            System.out.print("  ");
            for (int row = 1; row <= rowsToPrint; ++row) {
                String name = getCharForNumber(col) + String.valueOf(row);
                printCell(workBook, workSheet, name);
            }
            System.out.println("");
        }
    }

    private void printCell(String workBook, String workSheet, String name) {
        try {
            System.out.print(app.getValueOf(workBook, workSheet, name));
            System.out.print(" ");
        } catch (Exception e) {
            System.out.print(" ");
        }
    }

    private void addPrintBooksAction() {
        /*workBooks*/
        actions.put("workBooks", new Function() {
            @Override
            public Object apply(Object obj) {
                for (String book : app.getBooksNames()) {
                    System.out.print(book + "\n");
                }
                return null;
            }
        });
    }

    private void addPrintSheetsOfBookAction() {
        /*workSheetsOf workBook*/
        actions.put("workSheetsOf", new Function() {
            @Override
            public Object apply(Object obj) {
                String arguments = (String) obj;
                String[] args = arguments.split("-");
                // String workBook = args[0];
                for (String sheet : app.getSheetsNamesOfBook(args[0])) {
                    System.out.print(sheet + "\n");
                }
                return null;
            }
        });
    }

    private void addPrintCellValueAction() {
        /*getCellValue workBook-workSheet-cellName-value*/
        actions.put("getCellValue", new Function() {
            @Override
            public Object apply(Object obj) {
                String arguments = (String) obj;
                String[] args = arguments.split("-");
                //String workBook = args[0];
                //String workSheet = args[1];
                //String cellName = args[2];
                try {
                    System.out.print(app.getCellContentAsString(args[0], args[1], args[2]));
                } catch (NoValueException e) {
                    System.out.print(" \n");
                } catch (Exception e) {
                    System.out.print(e.getMessage());
                }
                return null;
            }
        });
    }


    /**
     * Returns the Alfabet Leter Corresponding to the @number.
     *
     * @param number a number
     * @return a Letter
     */
    private String getCharForNumber(int number) {
        return number > 0 && number < 27 ? String.valueOf((char) (number + 'A' - 1)) : null;
    }
}
