package ar.fiuba.tdd.tp1.persistence;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.Sheet;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.model.formulas.CellContent;
import ar.fiuba.tdd.tp1.model.types.ContentType;


import java.io.*;


/**
 * Created by ale on 10/19/15.
 */
public class JsonParser implements PersistenceParser {

    private Gson gson = null;
    //private String outName = "out.json";

    public JsonParser() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting().serializeNulls().registerTypeAdapter(Sheet.class, new InterfaceSerializer<>());
        //builder.registerTypeAdapter(SpreadSheetApplication.class,new SpreadSheetAppDeserializer());
        builder.registerTypeAdapter(Book.class, new BookDeserializer());
        builder.registerTypeAdapter(CellContent.class, new CellContentDeserializer());
        builder.registerTypeAdapter(ContentType.class, new ContentTypeSerializer());
        gson = builder.create();
    }

    @Override
    public void dump(SpreadSheetApplication app, String outPutFileName) {
        dumpSpreadSheetJson(app, outPutFileName);
    }

    private void dumpToJson(Object obj, String outPutFileName) {
        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(outPutFileName), "utf-8"));
            writer.write(gson.toJson(obj));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void dumpSpreadSheetJson(SpreadSheetApplication app, String outPutFileName) {
        dumpToJson(app, outPutFileName);
    }


    public SpreadSheetApplication retrieveSpreadSheet(String jsonFileName) {
        SpreadSheetApplication retrievedApp = null;
        try {
            Reader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(jsonFileName), "utf-8"));
            retrievedApp = gson.fromJson(reader, SpreadSheetApplication.class);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return retrievedApp;
    }

    @Override
    public void dumpBook(SpreadSheetApplication app, String bookName, String outPutFileName) throws NoBookException {
        dumpBookJson(app, bookName, outPutFileName);
    }

    public void dumpBookJson(SpreadSheetApplication app, String bookName, String outPutFileName) throws NoBookException {
        dumpToJson(app.getBook(bookName), outPutFileName);
    }


    public Book retrieveBook(String jsonFileName) {
        Book retrievedBook = null;
        try {
            Reader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(jsonFileName), "utf-8"));
            retrievedBook = gson.fromJson(reader, Book.class);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return retrievedBook;
    }
}
