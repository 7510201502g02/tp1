package ar.fiuba.tdd.tp1.persistence;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import ar.fiuba.tdd.tp1.model.types.ContentType;


import java.lang.reflect.Type;

/**
 * Created by ale on 10/26/15.
 */
public class ContentTypeSerializer implements JsonSerializer<ContentType> {
    /**
     * Para serializar la clase ContentType.
     *
     * @param src       the class to serialize.
     * @param typeOfSrc the ContentType type object.
     * @param context   Gson Builder settings.
     * @return
     */
    @Override
    public JsonElement serialize(ContentType src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getType());
    }
}
