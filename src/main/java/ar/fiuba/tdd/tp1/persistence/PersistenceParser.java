package ar.fiuba.tdd.tp1.persistence;

import ar.fiuba.tdd.tp1.exceptions.NoBookException;
import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;

/**
 * Created by ale on 10/26/15.
 */
public interface PersistenceParser {

    public void dump(SpreadSheetApplication app, String outPutFileName);

    public SpreadSheetApplication retrieveSpreadSheet(String persistedFileName);

    public void dumpBook(SpreadSheetApplication app, String bookName, String outPutFileName) throws NoBookException;

    public Book retrieveBook(String persistedFileName);
}
