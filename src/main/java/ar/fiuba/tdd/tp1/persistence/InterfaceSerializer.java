package ar.fiuba.tdd.tp1.persistence;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by ale on 10/19/15.
 */
public class InterfaceSerializer<T> implements JsonSerializer<T> {
    /**
     * /**
     * Para serializar clases que implementan interfaces.
     *
     * @param link    the template class to serialize.
     * @param type    the type of the abstract implementation.
     * @param context Gson Builder settings.
     * @return a JsonElement
     */
    public JsonElement serialize(T link, Type type,
                                 JsonSerializationContext context) {
        // Odd Gson quirk
        // not smart enough to use the actual type rather than the interface
        return context.serialize(link, link.getClass());
    }
}
