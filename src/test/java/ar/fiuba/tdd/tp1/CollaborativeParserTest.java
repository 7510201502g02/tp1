package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.model.Book;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.parsers.CollaborativeParser;
import ar.fiuba.tdd.tp1.parsers.CollaborativeParserOrderer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CollaborativeParserTest {

    private static final double DELTA = 0.0001;
    private CollaborativeParser collaborativeParser;
    private SpreadSheetApplication app;

    @Before
    public void setUp() throws Exception {
        app = new SpreadSheetApplication();
        app.addBook("book1");
        app.addSheet("book1", "sheet1");
        app.addSheet("book1", "sheet2");
        app.addSheet("book1", "sheet3");
        Book book = app.getBook("book1");
        collaborativeParser = new CollaborativeParserOrderer().getCollaborativeParser(book);
    }

    @After
    public void tearDown() throws Exception {
        app.restart();
    }

    @Test
    public void testParseValueFormula() {

        try {
            assertEquals("Value 2 parse Error.", 2, collaborativeParser.parse("2").eval());
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    public void testParseOtherValueFormula() {
        try {
            assertEquals("Value 5 parse Error.", 5, collaborativeParser.parse("5").eval());
        } catch (Exception e) {
            assert false;
        }
    }

    @Test(expected = Exception.class)
    public void testParseInvalidExpressionToFormula() throws Exception {
        collaborativeParser.parse("= iuzdsfgios + 068j").eval();
    }

    @Test
    public void testParseValueAddition() throws Exception {
        assertEquals("2 + 2 Parse OK", (double) 4, collaborativeParser.parse("= 2 + 2").eval());
    }

    @Test
    public void testParseValueSubstraction() throws Exception {
        assertEquals("8 - 2 Parse OK", (double) 6, collaborativeParser.parse("= 8 -2").eval());
    }

    @Test
    public void testParseCellReference() throws Exception {
        app.setFormulaOf("book1", "sheet3", "A6", "9");
        app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6");
        assertEquals(9, app.getValueOf("book1", "sheet1", "C3"), DELTA);
    }

    @Test
    public void testParseCellReferenceAdditionWhitValue() throws Exception {
        app.setFormulaOf("book1", "sheet3", "A6", "9");
        app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 + 2");
        assertEquals(11, app.getValueOf("book1", "sheet1", "C3"), DELTA);
    }

    @Test
    public void testParseCellReferenceSubstractionWhitValue() throws Exception {
        app.setFormulaOf("book1", "sheet3", "A6", "9");
        app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 - 2");
        assertEquals(7, app.getValueOf("book1", "sheet1", "C3"), DELTA);
    }

    @Test
    public void testParseCellReferenceAddition() throws Exception {
        app.setFormulaOf("book1", "sheet3", "A6", "9");
        app.setFormulaOf("book1", "sheet1", "C2", "18");
        app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 + !sheet1.C2");
        assertEquals(27, app.getValueOf("book1", "sheet1", "C3"), DELTA);
    }

    @Test
    public void testParseCellReferenceSubstraction() throws Exception {
        app.setFormulaOf("book1", "sheet3", "A6", "9");
        app.setFormulaOf("book1", "sheet1", "C2", "18");
        app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 - !sheet1.C2");
        assertEquals(-9, app.getValueOf("book1", "sheet1", "C3"), DELTA);
    }

    @Test
    public void testParseCellComplexOperation() throws Exception {
        app.setFormulaOf("book1", "sheet3", "A6", "1");
        app.setFormulaOf("book1", "sheet1", "C2", "2");
        app.setFormulaOf("book1", "sheet1", "C3", "= !sheet3.A6 + 4 - !sheet1.C2 + 1");
        assertEquals(4, app.getValueOf("book1", "sheet1", "C3"), DELTA);
    }


}