package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.model.Cell;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CellTest {

    private Cell cell;

    @Before
    public void setUp() {
        cell = new Cell("A2");
    }

    @Test
    public void testGetName() throws Exception {
        assertNotNull(cell.getName());
        assertEquals("A2", cell.getName());
    }


    // TODO : testear el comportamiento de las celdas! y de toda la estructura tambien!
}