package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.view.ConsoleView;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ale on 10/22/15.
 */
public class ConsoleViewTest {
    private SpreadSheetApplication app;
    private ConsoleView view;

    @Before
    public void setUp() throws Exception {
        app = new SpreadSheetApplication();
        app.addBook("book1");
        app.addSheet("book1", "sheet1");

        this.view = new ConsoleView(app);
    }

    @After
    public void tearDown() throws Exception {
        app.restart();
    }

    /*------------------------------------------------------------------------------
           TESTEO DEL SET FORMULA Y SU COMANDO
    -------------------------------------------------------------------------------*/

    @Test
    public void testLoco() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "2");

        //view.init();
        assertEquals(true, true);
    }
}
