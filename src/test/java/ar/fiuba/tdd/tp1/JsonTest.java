package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import ar.fiuba.tdd.tp1.persistence.JsonParser;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * Created by ale on 10/3/15.
 */
public class JsonTest {

    private static final double DELTA = 0.0001;
    JsonParser parser;
    private SpreadSheetApplication app;

    @Before
    public void setUp() throws Exception {
        parser = new JsonParser();

        app = new SpreadSheetApplication();
        app.addBook("book1");
    }

    @Test
    public void testDumpEmptySpreadsheet() throws Exception {
        app.setFormulaOf("book1", "default", "A1", "1235");
        app.setFormatterOf("book1", "default", "A1", "decimal", "2");
        app.setTypeOf("book1", "default", "A1", "Number");

        app.setFormulaOf("book1", "default", "A2", "2");
        app.setTypeOf("book1", "default", "A2", "Currency");
        app.setFormatterOf("book1", "default", "A2", "symbol", "U$S");
        app.setFormatterOf("book1", "default", "A2", "decimal", "2");

        app.setFormulaOf("book1", "default", "A3", "= 4+-2");

        app.setFormulaOf("book1", "default", "A4", "= 2+A3");

        assertEquals("1,235.00", app.getCellContentAsString("book1", "default", "A1"));
        assertEquals("U$S 2.00", app.getCellContentAsString("book1", "default", "A2"));
        assertEquals(2, app.getValueOf("book1", "default", "A3"), DELTA);
        assertEquals(4, app.getValueOf("book1", "default", "A4"), DELTA);


        parser.dumpSpreadSheetJson(app, "out.json");
    }

    @Test
    public void testRetrieveBasicSpreadSheet() throws Exception {
        String dumpFile = "jsonTests/testRetrieveBasicSpreadSheet.json";


        app.setFormulaOf("book1", "default", "A1", "1235");

        assertEquals(1235, app.getValueOf("book1", "default", "A1"), DELTA);

        parser.dumpSpreadSheetJson(app, dumpFile);

        SpreadSheetApplication retrievedApp = parser.retrieveSpreadSheet(dumpFile);

        assertEquals("1235", retrievedApp.getCellContentAsString("book1", "default", "A1"));

        String reDumpFile = "jsonTests/testRetrieveBasicSpreadSheetREDUMP.json";
        parser.dumpSpreadSheetJson(retrievedApp, reDumpFile);

        assertEquals(new String(Files.readAllBytes(Paths.get(dumpFile)), StandardCharsets.UTF_8),
                new String(Files.readAllBytes(Paths.get(reDumpFile)), StandardCharsets.UTF_8));
    }

    @Test
    public void testRetrieveSpreadSheetWithCellReferencesOfSameSheet() throws Exception {


        app.setFormulaOf("book1", "default", "A3", "= 4+-2");

        app.setFormulaOf("book1", "default", "A4", "= 2+A3");

        assertEquals(2, app.getValueOf("book1", "default", "A3"), DELTA);
        assertEquals(4, app.getValueOf("book1", "default", "A4"), DELTA);

        String dumpFile = "jsonTests/testRetrieveSpreadSheetWithCellReferencesOfSameSheet.json";

        parser.dumpSpreadSheetJson(app, dumpFile);

        SpreadSheetApplication retrievedApp = parser.retrieveSpreadSheet(dumpFile);

        assertEquals("= 4+-2", retrievedApp.getFormulaAsString("book1", "default", "A3"));
        assertEquals(2, retrievedApp.getValueOf("book1", "default", "A3"), DELTA);

        assertEquals("= 2+A3", retrievedApp.getFormulaAsString("book1", "default", "A4"));
        assertEquals(4, retrievedApp.getValueOf("book1", "default", "A4"), DELTA);

        String reDumpFile = "jsonTests/testRetrieveSpreadSheetWithCellReferencesOfSameSheetREDUMP.json";
        parser.dumpSpreadSheetJson(retrievedApp, reDumpFile);

        assertEquals(new String(Files.readAllBytes(Paths.get(dumpFile)), StandardCharsets.UTF_8),
                new String(Files.readAllBytes(Paths.get(reDumpFile)), StandardCharsets.UTF_8));
    }

    @Test
    public void testRetrieveSpreadSheetWithCellReferencesOfDistinctSheet() throws Exception {


        app.setFormulaOf("book1", "default", "A3", "= 4+-2");

        app.addSheet("book1", "sheet1");

        app.setFormulaOf("book1", "sheet1", "A3", "= 2+!default.A3");

        assertEquals(2, app.getValueOf("book1", "default", "A3"), DELTA);
        assertEquals(4, app.getValueOf("book1", "sheet1", "A3"), DELTA);

        String dumpFile = "jsonTests/testRetrieveSpreadSheetWithCellReferencesOfDistinctSheet.json";

        parser.dumpSpreadSheetJson(app, dumpFile);

        SpreadSheetApplication retrievedApp = parser.retrieveSpreadSheet(dumpFile);

        assertEquals("= 4+-2", retrievedApp.getFormulaAsString("book1", "default", "A3"));
        assertEquals(2, retrievedApp.getValueOf("book1", "default", "A3"), DELTA);

        assertEquals("= 2+!default.A3", retrievedApp.getFormulaAsString("book1", "sheet1", "A3"));
        assertEquals(4, retrievedApp.getValueOf("book1", "sheet1", "A3"), DELTA);

        String reDumpFile = "jsonTests/testRetrieveSpreadSheetWithCellReferencesOfDistinctSheetREDUMP.json";
        parser.dumpSpreadSheetJson(retrievedApp, reDumpFile);

        assertEquals(new String(Files.readAllBytes(Paths.get(dumpFile)), StandardCharsets.UTF_8),
                new String(Files.readAllBytes(Paths.get(reDumpFile)), StandardCharsets.UTF_8));
    }

    @Test
    public void testRetrieveSpreadSheetWithOneFormat() throws Exception {

        app.setFormulaOf("book1", "default", "A1", "1235");
        app.setFormatterOf("book1", "default", "A1", "decimal", "2");

        assertEquals("1,235.00", app.getCellContentAsString("book1", "default", "A1"));

        String dumpFile = "jsonTests/testRetrieveSpreadSheetWithOneFormat.json";
        parser.dumpSpreadSheetJson(app, dumpFile);

        SpreadSheetApplication retrievedApp = parser.retrieveSpreadSheet(dumpFile);

        assertEquals("1,235.00", retrievedApp.getCellContentAsString("book1", "default", "A1"));

        String reDumpFile = "jsonTests/testRetrieveSpreadSheetWithOneFormatREDUMP.json";
        parser.dumpSpreadSheetJson(retrievedApp, reDumpFile);

        assertEquals(new String(Files.readAllBytes(Paths.get(dumpFile)), StandardCharsets.UTF_8),
                new String(Files.readAllBytes(Paths.get(reDumpFile)), StandardCharsets.UTF_8));
    }

    @Test
    public void testRetrieveSpreadSheetWithSeveralFormats() throws Exception {

        app.setFormulaOf("book1", "default", "A1", "123569");
        app.setFormatterOf("book1", "default", "A1", "decimal", "2");
        app.setFormatterOf("book1", "default", "A1", "symbol", "U$S");

        assertEquals("U$S 123,569.00", app.getCellContentAsString("book1", "default", "A1"));

        String dumpFile = "jsonTests/testRetrieveSpreadSheetWithSeveralFormats.json";


        parser.dumpSpreadSheetJson(app, dumpFile);

        SpreadSheetApplication retrievedApp = parser.retrieveSpreadSheet(dumpFile);

        assertEquals("U$S 123,569.00", retrievedApp.getCellContentAsString("book1", "default", "A1"));

        String reDumpFile = "jsonTests/testRetrieveSpreadSheetWithSeveralFormatsREDUMP.json";
        parser.dumpSpreadSheetJson(retrievedApp, reDumpFile);

        assertEquals(new String(Files.readAllBytes(Paths.get(dumpFile)), StandardCharsets.UTF_8),
                new String(Files.readAllBytes(Paths.get(reDumpFile)), StandardCharsets.UTF_8));
    }

    @Test
    public void testRetrieveSpreadSheetWithReferencesAndFormats() throws Exception {


        app.setFormulaOf("book1", "default", "A2", "4");

        app.setFormulaOf("book1", "default", "A1", "= 123560 + A2");
        app.setFormatterOf("book1", "default", "A1", "decimal", "2");
        app.setFormatterOf("book1", "default", "A1", "symbol", "U$S");

        assertEquals("U$S 123,564.00", app.getCellContentAsString("book1", "default", "A1"));

        String dumpFile = "jsonTests/testRetrieveSpreadSheetWithReferencesAndFormats.json";

        parser.dumpSpreadSheetJson(app, dumpFile);

        SpreadSheetApplication retrievedApp = parser.retrieveSpreadSheet(dumpFile);

        assertEquals("U$S 123,564.00", retrievedApp.getCellContentAsString("book1", "default", "A1"));

        String reDumpFile = "jsonTests/testRetrieveSpreadSheetWithReferencesAndFormatsREDUMP.json";
        parser.dumpSpreadSheetJson(retrievedApp, reDumpFile);

        assertEquals(new String(Files.readAllBytes(Paths.get(dumpFile)), StandardCharsets.UTF_8),
                new String(Files.readAllBytes(Paths.get(reDumpFile)), StandardCharsets.UTF_8));
    }

    @Test
    public void testRetrieveSpreadSheetWithTypes() throws Exception {


        app.setFormulaOf("book1", "default", "A1", "1235");
        app.setTypeOf("book1", "default", "A1", "Number");

        app.setFormulaOf("book1", "default", "A2", "2");
        app.setTypeOf("book1", "default", "A2", "Currency");

        app.setFormulaOf("book1", "default", "A3", "= 1 + 2");
        app.setTypeOf("book1", "default", "A3", "String");

        assertEquals("1235", app.getCellContentAsString("book1", "default", "A1"));
        assertEquals("2", app.getCellContentAsString("book1", "default", "A2"));
        assertEquals("= 1 + 2", app.getCellContentAsString("book1", "default", "A3"));

        String dumpFile = "jsonTests/testRetrieveSpreadSheetWithTypes.json";

        parser.dumpSpreadSheetJson(app, dumpFile);

        SpreadSheetApplication retrievedApp = parser.retrieveSpreadSheet(dumpFile);

        assertEquals("1235", retrievedApp.getCellContentAsString("book1", "default", "A1"));
        assertEquals("2", retrievedApp.getCellContentAsString("book1", "default", "A2"));
        assertEquals("= 1 + 2", retrievedApp.getCellContentAsString("book1", "default", "A3"));

        String reDumpFile = "jsonTests/testRetrieveSpreadSheetWithTypesREDUMP.json";
        parser.dumpSpreadSheetJson(retrievedApp, reDumpFile);

        assertEquals(new String(Files.readAllBytes(Paths.get(dumpFile)), StandardCharsets.UTF_8),
                new String(Files.readAllBytes(Paths.get(reDumpFile)), StandardCharsets.UTF_8));
    }

    @Test
    public void testRetrieveSpreadSheetWithTypesFormattsAndReferences() throws Exception {
        app.setFormulaOf("book1", "default", "A1", "1230");
        app.setFormatterOf("book1", "default", "A1", "decimal", "2");
        app.setTypeOf("book1", "default", "A1", "Number");

        app.setFormulaOf("book1", "default", "A2", "2");
        app.setTypeOf("book1", "default", "A2", "Currency");
        app.setFormatterOf("book1", "default", "A2", "symbol", "U$S");
        app.setFormatterOf("book1", "default", "A2", "decimal", "2");

        app.setFormulaOf("book1", "default", "A3", "= 4.06+A1");
        app.setFormatterOf("book1", "default", "A3", "decimal", "0");
        app.setTypeOf("book1", "default", "A3", "Number");

        app.setFormulaOf("book1", "default", "A4", "= 2+A3");
        app.setFormatterOf("book1", "default", "A4", "decimal", "2");
        app.setTypeOf("book1", "default", "A4", "Number");
        app.setFormatterOf("book1", "default", "A4", "symbol", "U$S");

        assertEquals("1,230.00", app.getCellContentAsString("book1", "default", "A1"));
        assertEquals("U$S 2.00", app.getCellContentAsString("book1", "default", "A2"));
        assertEquals("1,234", app.getCellContentAsString("book1", "default", "A3"));
        assertEquals("U$S 1,236.06", app.getCellContentAsString("book1", "default", "A4"));

        String dumpFile = "jsonTests/testRetrieveSpreadSheetWithTypesFormattsAndReferences.json";

        parser.dumpSpreadSheetJson(app, dumpFile);

        SpreadSheetApplication retrievedApp = parser.retrieveSpreadSheet(dumpFile);

        assertEquals("1,230.00", retrievedApp.getCellContentAsString("book1", "default", "A1"));
        assertEquals("U$S 2.00", retrievedApp.getCellContentAsString("book1", "default", "A2"));
        assertEquals("1,234", retrievedApp.getCellContentAsString("book1", "default", "A3"));
        assertEquals("U$S 1,236.06", retrievedApp.getCellContentAsString("book1", "default", "A4"));

        String reDumpFile = "jsonTests/testRetrieveSpreadSheetWithTypesFormattsAndReferencesREDUMP.json";
        parser.dumpSpreadSheetJson(retrievedApp, reDumpFile);

        assertEquals(new String(Files.readAllBytes(Paths.get(dumpFile)), StandardCharsets.UTF_8),
                new String(Files.readAllBytes(Paths.get(reDumpFile)), StandardCharsets.UTF_8));
    }
}
