package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exceptions.NoSheetsException;
import ar.fiuba.tdd.tp1.model.SpreadSheetApplication;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by ale on 10/3/15.
 */
public class SpreadSheetAppTest {

    private SpreadSheetApplication app;

    @Before
    public void setUp() throws Exception {
        app = new SpreadSheetApplication();
        app.addBook("book1");
        app.addSheet("book1", "sheet1");
    }

    @After
    public void tearDown() throws Exception {
        app.restart();
    }

    /*------------------------------------------------------------------------------
           TESTEO DEL SET FORMULA Y SU COMANDO
    -------------------------------------------------------------------------------*/

    @Test
    public void testObtainSimpleValue() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2);
    }

    @Test
    public void testUndoFormulaSet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "2");
        app.setFormulaOf("book1", "sheet1", "A1", "8");

        assertEquals(8, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.undo();
        assertEquals(2, (int) app.getValueOf("book1", "sheet1", "A1"));
    }

    @Test
    public void testDoubleUndoFormulaSet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "6");
        app.setFormulaOf("book1", "sheet1", "A1", "9");
        app.setFormulaOf("book1", "sheet1", "A1", "15");

        assertEquals(15, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.undo();
        assertEquals(9, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.undo();
        assertEquals(6, (int) app.getValueOf("book1", "sheet1", "A1"));
    }

    @Test
    public void testRedoFormulaSet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "6");
        app.setFormulaOf("book1", "sheet1", "A1", "9");

        assertEquals(9, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.undo();
        assertEquals("Falla el undo", 6, (int) app.getValueOf("book1", "sheet1", "A1"));
        app.redo();
        assertEquals("Falla el redo", 9, (int) app.getValueOf("book1", "sheet1", "A1"));
    }

    /*------------------------------------------------------------------------------
            TESTEO DEL NEW SHEET Y SU COMANDO
     -------------------------------------------------------------------------------*/

    @Test
    public void testAddSheet() throws Exception {
        try {
            app.setFormulaOf("book1", "sheet1", "H7", "9");
        } catch (NoSheetsException e) {
            fail("No pude crear otra hoja");
        }

        assertEquals("No pude crear otra hoja", 9, (int) app.getValueOf("book1", "sheet1", "H7"));
    }

    @Test(expected = NoSheetsException.class)
    public void testUndoAddSheet() throws Exception {
        app.addSheet("book1", "sheet2");
        try {
            app.setFormulaOf("book1", "sheet2", "H7", "9");
        } catch (NoSheetsException e) {
            fail("No pude crear otra hoja");
        }

        assertEquals("No pude crear otra hoja", 9, (int) app.getValueOf("book1", "sheet2", "H7"));

        app.undo(); //undo del setFormulaOf(2, "H7", "9");
        app.undo(); //undo del addSheet()

        app.getValueOf("book1", "sheet2", "H7");//Deberia lanzar NoSheetException
    }

    @Test
    public void testRedoAddSheet() throws Exception {
        try {
            app.setFormulaOf("book1", "sheet1", "H7", "9");
        } catch (NoSheetsException e) {
            fail("No pude crear otra hoja");
        }
        assertEquals("No pude crear otra hoja", 9, (int) app.getValueOf("book1", "sheet1", "H7"));

        app.undo(); //undo del setFormulaOf(2, "H7", "9");
        app.undo(); //undo del addSheet()

        app.redo(); //se vuelve a crear la sheet
        app.setFormulaOf("book1", "sheet1", "H7", "65");
        assertEquals("El redo de addSheet NO funciona", 65, (int) app.getValueOf("book1", "sheet1", "H7"));
    }

     /*------------------------------------------------------------------------------
            TESTEO DEL REMOVE SHEET Y SU COMANDO
     -------------------------------------------------------------------------------*/

    @Test(expected = NoSheetsException.class)
    public void testRemoveFirstSheet() throws Exception {
        app.removeSheet("book1", "sheet1");
        app.setFormulaOf("book1", "sheet1", "H7", "9"); //Deberia lanzar NoSheetException
    }

    @Test(expected = NoSheetsException.class)
    public void testRemoveSomeSheet() throws Exception {
        app.addSheet("book1", "sheet2");
        app.addSheet("book1", "sheet3");
        app.removeSheet("book1", "sheet3");
        //Se corren todas para la izquierda
        app.setFormulaOf("book1", "sheet3", "H7", "9"); //Deberia lanzar NoSheetException
    }

    @Test
    public void testUndoRemoveSheet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "H7", "9");
        app.removeSheet("book1", "sheet1");
        app.undo();
        //La MISMA hoja que saco vuelve a entrar
        assertEquals("El undo de removeSheet NO funciona", 9, (int) app.getValueOf("book1", "sheet1", "H7"));
    }

    @Test(expected = NoSheetsException.class)
    public void testRedoRemoveSheet() throws Exception {
        app.setFormulaOf("book1", "sheet1", "H7", "9");
        app.removeSheet("book1", "sheet1");
        app.undo(); // Vuelve a dejar la hoja
        app.redo(); // Vuelva a SACAR la hoja
        app.setFormulaOf("book1", "sheet1", "H7", "9"); //Deberia lanzar NoSheetException
    }


     /*------------------------------------------------------------------------------
            TESTEO DE ARITMETICA DE CELDAS
     -------------------------------------------------------------------------------*/

    @Test
    public void testObtainResultSum() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 2 + 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2 + 2);
    }

    @Test
    public void testObtainResultSubtract() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 2 - 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2 - 2);
    }

    @Test
    public void testResultSumWithCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 + 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 5 + 2);
    }

    @Test
    public void testResultSubtractWithCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "5");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 - 2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 5 - 2);
    }

    @Test
    public void testResultMultiSum() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 2 + 2 + 4 + 5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2 + 2 + 4 + 5);
    }

    @Test
    public void testResultMultiSubtract() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 2 - 2 - 4 - 5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 2 - 2 - 4 - 5);
    }

    @Test
    public void testResultMultiSumWithCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 + 2 + 2 + 4 + 5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 + 2 + 2 + 4 + 5);
    }

    @Test
    public void testResultMultiSubtractWithCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 - 2 - 2 - 4 - 5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 - 2 - 2 - 4 - 5);
    }

    @Test
    public void testResultMultiCellSum() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "5");
        app.setFormulaOf("book1", "sheet1", "D3", "1");
        app.setFormulaOf("book1", "sheet1", "G5", "-3");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 + !sheet1.C10 + !sheet1.D3 + !sheet1.G5");
        assertEquals((10 + 5 + 1 + (-3)), (int) app.getValueOf("book1", "sheet1", "A1"));
    }

    @Test
    public void testResultMultiCellSubtract() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "5");
        app.setFormulaOf("book1", "sheet1", "D3", "1");
        app.setFormulaOf("book1", "sheet1", "G5", "-3");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 - !sheet1.C10 - !sheet1.D3 - !sheet1.G5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 - 5 - 1 - (-3));
    }

    @Test
    public void testResultMultiOperand() throws Exception {
        app.setFormulaOf("book1", "sheet1", "A1", "= 33 + 5 - 100 + 8 - 10");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 33 + 5 - 100 + 8 - 10);
    }

    @Test
    public void testResultMultiOperandCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "5");
        app.setFormulaOf("book1", "sheet1", "D3", "1");
        app.setFormulaOf("book1", "sheet1", "G5", "-3");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 - !sheet1.C10 + !sheet1.D3 - !sheet1.G5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 - 5 + 1 - (-3));
    }

    @Test
    public void testResultInterSheetCell() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "5");
        app.addSheet("book1", "sheet2");
        app.setFormulaOf("book1", "sheet2", "D3", "1");
        app.addSheet("book1", "sheet3");
        app.setFormulaOf("book1", "sheet3", "G5", "-3");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet1.B2 - !sheet1.C10 + !sheet2.D3 - !sheet3.G5");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), 10 - 5 + 1 - (-3));
    }

    @Test
    public void testResultInterSheetCellComposite() throws Exception {
        app.setFormulaOf("book1", "sheet1", "B2", "10");
        app.setFormulaOf("book1", "sheet1", "C10", "= 5 + !sheet1.B2");
        app.addSheet("book1", "sheet2");
        app.setFormulaOf("book1", "sheet2", "D3", "= 1 - !sheet1.C10");
        app.addSheet("book1", "sheet3");
        app.setFormulaOf("book1", "sheet3", "G5", "= -3 + !sheet2.D3");
        app.setFormulaOf("book1", "sheet1", "A1", "= !sheet3.G5 + !sheet1.B2");
        assertEquals((int) app.getValueOf("book1", "sheet1", "A1"), (-3) + (1 - (5 + 10)) + 10);
    }
}
