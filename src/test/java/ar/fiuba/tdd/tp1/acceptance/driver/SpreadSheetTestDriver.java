package ar.fiuba.tdd.tp1.acceptance.driver;

import java.util.List;

public interface SpreadSheetTestDriver {

    List<String> workBooksNames();

    void createNewWorkBookNamed(String name);

    void createNewWorkSheetNamed(String workbookName, String name);

    List<String> workSheetNamesFor(String workBookName);

    void setCellValue(String workBookName, String workSheetName, String cellId, String value);

    String getCellValueAsString(String workBookName, String workSheetName, String cellId) throws BadFormatException;

    double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) throws BadFormulaException, BadFormatException;

    void undo();

    void redo();

    void setCellType(String workBookName, String workSheetName, String cellId, String cellType);

    void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatterType, String formatterOption);

    void persistWorkBook(String bookName, String fileName);

    void reloadPersistedWorkBook(String fileName);

    void saveAsCSV(String workBookName, String workSheetName, String fileName);

    void loadFromCSV(String workBookName, String workSheetName, String fileName);
}