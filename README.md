# TP1

Forkear este proyecto y asignar al user tdd7510 como administrador.


------------------------------------------------------------
GRADLE
------------------------------------------------------------

All tasks runnable from root project
------------------------------------------------------------

Build tasks
-----------
assemble - Assembles the outputs of this project.
build - Assembles and tests this project.
buildDependents - Assembles and tests this project and all projects that depend on it.
buildNeeded - Assembles and tests this project and all projects it depends on.
classes - Assembles classes 'main'.
clean - Deletes the build directory.
jar - Assembles a jar archive containing the main classes.
testClasses - Assembles classes 'test'.

Build Setup tasks
-----------------
init - Initializes a new Gradle build. [incubating]
wrapper - Generates Gradle wrapper files. [incubating]

Documentation tasks
-------------------
javadoc - Generates Javadoc API documentation for the main source code.

Help tasks
----------
components - Displays the components produced by root project 'tp1'. [incubating]
dependencies - Displays all dependencies declared in root project 'tp1'.
dependencyInsight - Displays the insight into a specific dependency in root project 'tp1'.
help - Displays a help message.
model - Displays the configuration model of root project 'tp1'. [incubating]
projects - Displays the sub-projects of root project 'tp1'.
properties - Displays the properties of root project 'tp1'.
tasks - Displays the tasks runnable from root project 'tp1'.

IDE tasks
---------
cleanEclipse - Cleans all Eclipse files.
cleanIdea - Cleans IDEA project files (IML, IPR)
eclipse - Generates all Eclipse files.
idea - Generates IDEA project files (IML, IPR, IWS)

Verification tasks
------------------
check - Runs all checks.
test - Runs the unit tests.

Other tasks
-----------
cleanIdeaWorkspace
fatJar
findbugsTest - Run FindBugs analysis for test classes
install - Installs the 'archives' artifacts into the local Maven repository.
jacocoTestReport
pmdTest - Run PMD analysis for test classes

Rules
-----
Pattern: clean<TaskName>: Cleans the output files of a task.
Pattern: build<ConfigurationName>: Assembles the artifacts of a configuration.
Pattern: upload<ConfigurationName>: Assembles and uploads the artifacts belonging to a configuration.

To see all tasks and more detail, run gradlew tasks --all

To see more detail about a task, run gradlew help --task <task>
